*An ansible playbook for provisioning a Gitlab CE instance using omnibus package*

## Roles

### Gitlab - `gitlab`

Just loads the tasks from [`geerlingguy.gitlab`](https://github.com/geerlingguy/ansible-role-gitlab) role.

### Backup - `backup`

Adapt [`coopdevs.backup_role`](https://github.com/coopdevs/backups_role) to take Gitlab backups.

Changed default variables:
```yaml
# Set the paths where Gitlab store its own backups
backups_role_assets_paths: ['/etc/gitlab/config_backup/', '/var/opt/gitlab/backups']
# Grant permissions to use gitlab-ctl & gitlab-backup commands.
backups_role_sudoers_cmd_pattern: |
 '/bin/tar -czvf *, gitlab-ctl, gitlab-backup, cp'
```

A [template](roles/backups/templates/gb_backup.sh.j2) with the proper commands to take a backup from Gitlab also belongs to this role.

### Other community roles
- [coopdevs.sys-admins-role](https://github.com/coopdevs/sys-admins-role)
- [geerlingguy.security](https://github.com/geerlingguy/ansible-role-security)
- [paulfantom.restic](https://github.com/paulfantom/ansible-restic)

## Setup

### Configure python environment

In order to assure that the playbooks contained here are run always with the same ansible version and linted the same way, we are using [`pyenv`](https://github.com/pyenv/pyenv).

Follow [Installing and using pyenv](https://github.com/coopdevs/handbook/wiki/Installing-and-using-pyenv), or, in short:

```sh
pyenv install 3.8.12
pyenv virtualenv 3.8.12 gitlab-provisioning
```

### Configure ansible environment

You will need Ansible on your machine to run the playbooks, follow the steps below to install it.

```sh
pyenv exec pip install -r requirements.txt
ansible-galaxy install -r requirements.yml -f
```

### Install pre-commit hooks

We use [pre-commit framework](https://pre-commit.com/) to asure quality code.

```sh
pre-commit install
```

## Development

### Using lxc ( recommended for Linux )

* install devenv https://github.com/coopdevs/devenv
* run the following

```sh
cd /path/to/gitlab-provisioning
devenv
```

### Configure sysadmins users and execute provision

```sh
ansible-playbook playbooks/sys_admins.yml --limit=dev --user=root
ansible-playbook playbooks/provision.yml --limit=dev
```

## Test/Staging

You can find the vault pass in Coopdevs Bitwarden with the name:

`Gitlab provisioning test ansible secret`

### Configure sysadmins users

```sh
ansible-playbook playbooks/sys_admins.yml --limit=test --user=root --ask-vault-pass
```

### Execute provisioning and deploy

```sh
ansible-playbook playbooks/provision.yml --limit=test --ask-vault-pass
```
